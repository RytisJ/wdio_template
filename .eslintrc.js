module.exports = {
  'env': {
    'commonjs': true,
    'es2021': true,
    'node': true,
  },
  'extends': [
    'google',
    '@gaincompliance/gain/rules/tests/cucumber',
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
  },
  'plugins': [
    'cucumber',
  ],
  'rules': {

  },
};
