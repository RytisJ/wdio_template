const fsExtra = require('fs-extra');
const {generate} = require('multiple-cucumber-html-reporter');

const cucumberJson = require('wdio-cucumberjs-json-reporter').default;
const os = require('os');
const parseMS = require('pretty-ms');
const startTime = new Date();

require('dotenv').config({path: `.env.${process.env.NODE_ENV.trim()}`});

/**
 * returns config object for report
 * @return {object} config object
 */
function osName() {
  const platform = os.platform();
  switch (platform) {
    case 'win32':
      return 'windows';
    case 'darwin':
      return 'osx';
    case 'android':
    case 'linux':
      return platform;
    default:
      return '?';
  }
}

/**
 * creates extra config for report, adding run times
 * @return {object}
 */
function customDataGenerator() {
  const end = new Date();
  return [
    {label: 'start', value: `${startTime.toISOString()}`},
    {label: 'end', value: `${end.toISOString()}`},
    {label: 'total', value: `${parseMS(end - startTime)}`},
  ];
}

exports.config = {

  specs: [
    './test/features/**/*.feature',
  ],

  maxInstances: 1,

  capabilities: [{
    'maxInstances': 1,
    'browserName': 'chrome',
    'acceptInsecureCerts': true,
    'cjson:metadata': {
      device: os.hostname(),
      platform: {
        name: osName(),
        version: os.release(),
      },
    },
  }],
  // Level of logging verbosity: trace | debug | info | warn | error | silent
  logLevel: 'error',
  logLevels: {
    frameworkLogger: 'info',
  },
  bail: 0,
  baseUrl: '',
  //
  // Default timeout for all waitFor* commands.
  waitforTimeout: 10000,
  //
  // Default timeout in milliseconds for request
  // if browser driver or grid doesn't send response
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: ['chromedriver'],
  framework: 'cucumber',
  reporters: [
    'spec',
    ['cucumberjs-json', {
      jsonFolder: './reports',
      language: 'en',
    },
    ],
  ],
  cucumberOpts: {
    // <string[]> (file/dir) require files before executing features
    require: ['./test/step-definitions/**/*.steps.js'],
    backtrace: false,
    requireModule: [],
    dryRun: false,
    // <boolean> abort the run on first failure
    failFast: false,
    // <boolean> hide step definition snippets for pending steps
    snippets: true,
    // <boolean> hide source uris
    source: true,
    // <boolean> fail if there are any undefined or pending steps
    strict: false,
    tagExpression: process.argv.tags,
    timeout: 60000,
    ignoreUndefinedDefinitions: false,
  },

  before: function(config, capabilities) {
    fsExtra.emptyDirSync('./reports');
  },
  onComplete: function() {
    generate({
      jsonDir: './reports',
      reportPath: './reports',
      displayReportTime: true,
      openReportInBrowser: true,
      showExecutionTime: true,
      reportName: 'rytis',
      displayDuration: true,
      displayReportTime: true,
      customData: {
        title: 'Run info',
        data: customDataGenerator(),
      },
    });
  },
  afterStep: async function(step, scenario, result, context) {
    if (!result.passed) {
      console.log('FAILED');
      const img = await browser.takeScreenshot();
      cucumberJson.attach(img, 'image/png');
    }
  },
  // beforeScenario: function (world, context) {
  // },
  // afterFeature: function (uri, feature) {
  // },
};
