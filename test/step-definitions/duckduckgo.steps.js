const {Given, When, Then} = require('@wdio/cucumber-framework');
const DuckDuckGoPage = require('../pages/duckduckgo.page');

Given('I navigate to duckduckgo', async () => DuckDuckGoPage.open());

When('I search for {string}', async (searchTerm) =>
  DuckDuckGoPage.search(searchTerm));

Then('I am on right page', async () =>
  expect(await browser.getTitle()).toEqual('foobar at DuckDuckGo'));
