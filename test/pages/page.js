const logger = require('@wdio/logger').default;
const frameworkLogger = logger('frameworkLogger');

module.exports = class Page {
  log = frameworkLogger;

  /**
 * log out a message and attach it to report
 * @param {string} msg
 */
  logAndAttach(msg) {
    log.info(msg);
    cucumberJson.attach(msg);
  }
};
