const Page = require('./page');

/**
 * duck duck go search page
 */
class DuckDuckGoPage extends Page {
  /**
   * opens duckduckgo webpage
   */
  async open() {
    browser.url(process.env.url);
  }

  /**
   * duckduckgo search
   * @param {string} searchTerm
   */
  async search(searchTerm) {
    await $('#search_form_input_homepage').setValue(searchTerm);
    await $('#search_button_homepage').click();
  }
}

module.exports = new DuckDuckGoPage();
